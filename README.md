<h1 align="center"> <a href="https://www.linkedin.com/in/ericrodriguesfer/"> Eric Rodrigues Ferreira </a> 😆 </h1>

<!--<h1 align="center"> Sobre mim :sunglasses: </h1>
<p align="justify">
Sou Eric Rodrigues Ferreira, um brasileiro natural de Quixaramobim - Ceará. Sou fascinado por T.I, inovação, programação, desafios, tecnologias. Gosto de resolver problemas, sempre empregando as melhores técnicas e práticas possíveis para prover uma ótima solução. Meu conhecimento externo a faculdade e carreira acadêmica, foi angariado de estudos autodidata em meio ao mundo de conhecimentos grautitos que temos hoje ao acessar <a href="https://google.com" target="_blank"> Google.com</a>. Sou <b>Desenvolvedor Back-end</b> (:heart:), meus conhecimentos e estudos hoje estão em torno de <b>Javascript</b>, <b>Typescript</b>, <b>Node.JS</b>, <b>Expres.JS</b>, <b>Nest.JS</b>, <b>Spring Boot</b>, <b>MongoDB</b>, <b>PostgreSQL</b>, <b>MySQL</b>, <b>Docker</b>, <b>TDD</b>, <b>SOLID</b>, <b>Design Patterns</b> entre outros.
</p>-->

<!--<h1 align="center"> Linguagens e ferramentas</h1>-->
<!--<code><img alt="HTML5" src="https://img.shields.io/badge/html5-%23E34F26.svg?style=for-the-badge&logo=html5&logoColor=white"/></code>-->
<!--<code><img alt="CSS3" src="https://img.shields.io/badge/css3-%231572B6.svg?style=for-the-badge&logo=css3&logoColor=white"/></code>-->
<!--<code><img alt="JavaScript" src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black"/></code>
<code><img alt="Typescript" src="https://img.shields.io/badge/typescript-%23007ACC.svg?style=for-the-badge&logo=typescript&logoColor=white"/></code>
<code><img alt="ExpressJs" src="https://img.shields.io/badge/express.js-%23404d59.svg?style=for-the-badge&logo=express&logoColor=%2361DAFB"/></code>
<code><img alt="NestJs" src="https://img.shields.io/badge/nestjs-%23E0234E.svg?style=for-the-badge&logo=nestjs&logoColor=white"/></code>
<code><img alt="PHP" src="https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white"/></code>
<code><img alt="React" src="https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB"/></code>-->
<!--<code><img alt="NodeJS" src="https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white"/></code>
<code><img alt="Java" src="https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white"/></code>
<code><img alt="Spring boot" src="https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white"/></code>
<code><img alt="PostreSQL" src="https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white"/></code>
<code><img alt="MySQL" src="https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white"/></code>
<code><img alt="MongoDB" src="https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white"/></code>
<code><img alt="Docker" src="https://img.shields.io/badge/Docker-2CA5E0?style=for-the-badge&logo=docker&logoColor=white"/></code>
<code><img alt="Git" src="https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white"/></code>
<code><img alt="GitHub" src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white"/></code>
<code><img alt="GitLab" src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white"/></code>
<code><img alt="Linux" src="https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black"/></code>
<code><img alt="Pop OS" src="https://img.shields.io/badge/Pop!_OS-FF7139?style=for-the-badge&logo=Pop!_OS&logoColor=white"/></code>-->

<h1 align="left"> Fale comigo :speech_balloon: </h1>

[![Linkedin Badge](https://img.shields.io/badge/ericrodriguesferreira-%230077B5.svg?style=for-the-badge&logo=linkedin&logoColor=white&link=https://www.linkedin.com/in/ericrodriguesfer/)](https://www.linkedin.com/in/ericrodriguesfer/) [![Gmail Badge](https://img.shields.io/badge/ericdesenvolvedor7@gmail.com-0078D4?style=for-the-badge&logo=Gmail&logoColor=white&link=mailto:ericdesenvolvedor7@gmail.com)](mailto:ericdesenvolvedor7@gmail.com)
  
<br>

<h1 align="left"> Veja mais em 👀 </h1>

[![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://github.com/ericrodriguesfer)

<br>

<!--<h3 align="center"> Fique livre para 🔎 os repositórios! </h3>
<h3 align="center"> Deixe uma ⭐ no que você mais gostou! </h3>-->
